package info.guardianproject.keanu.core.util

import android.app.Activity
import android.content.ComponentName
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.graphics.Bitmap
import android.net.Uri
import android.os.Parcelable
import android.provider.MediaStore
import androidx.activity.result.ActivityResult
import androidx.activity.result.ActivityResultCallback
import androidx.appcompat.app.AlertDialog
import androidx.core.content.FileProvider
import com.theartofdev.edmodo.cropper.CropImageView
import info.guardianproject.keanuapp.R
import timber.log.Timber
import java.io.File
import java.io.IOException
import java.lang.ref.WeakReference

class ImageChooserHelper(context: Context) {

    private val mContext = WeakReference(context)

    /**
     * Get URI to image received from capture by camera.
     */
    private val mCaptureOutputUri: Uri?
        get() {
            val context = mContext.get() ?: return null

            if (context.externalCacheDir == null) return null

            return FileProvider.getUriForFile(context,
                    "${context.packageName}.provider",
                    File(context.externalCacheDir, "pickImageResult.jpg"))
        }

    /**
     * Create a chooser intent to select the source to get an image from.
     *
     * The source can be camera's (ACTION_IMAGE_CAPTURE) or gallery's (ACTION_GET_CONTENT).
     *
     * All possible sources are added to the intent chooser.
     *
     * @return A chooser [Intent]
     */
    val intent: Intent
        get() {
            val allIntents = java.util.ArrayList<Intent>()

            // Collect all camera intents, if a `captureOutput` is provided.
            mCaptureOutputUri?.let {
                val captureIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)

                mContext.get()?.packageManager?.queryIntentActivities(captureIntent, 0)?.forEach { info ->
                    val intent = Intent(captureIntent)
                    intent.component = ComponentName(info.activityInfo.packageName, info.activityInfo.name)
                    intent.setPackage(info.activityInfo.packageName)
                    intent.putExtra(MediaStore.EXTRA_OUTPUT, it)

                    allIntents.add(intent)
                }
            }

            // Collect all gallery intents.
            val galleryIntent = Intent(Intent.ACTION_GET_CONTENT)
            galleryIntent.type = "image/*"

            mContext.get()?.packageManager?.queryIntentActivities(galleryIntent, 0)?.forEach { info ->
                val intent = Intent(galleryIntent)
                intent.component = ComponentName(info.activityInfo.packageName, info.activityInfo.name)
                intent.setPackage(info.activityInfo.packageName)

                allIntents.add(intent)
            }

            // The main intent is the last in the list (fucking Android) so pickup the useless one.
            val mainIntent = allIntents.firstOrNull { it.component?.className == "com.android.documentsui.DocumentsActivity" }
                    ?: allIntents.last()

            allIntents.remove(mainIntent)

            // Create a chooser from the main intent.
            val chooserIntent = Intent.createChooser(mainIntent, mContext.get()?.getString(R.string.choose_photos))

            // Add all other intents.
            chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, allIntents.toTypedArray<Parcelable>())

            return chooserIntent
        }

    /**
     * Get the URI of the selected image from [.getPickImageChooserIntent].
     *
     * @param data the returned data of the activity result
     * @return the correct URI for camera and gallery image.
     */
    private fun getPickImageResult(data: Intent?): Uri? {
        if (data?.data != null && data.action != MediaStore.ACTION_IMAGE_CAPTURE) {
            return data.data
        }

        return mCaptureOutputUri
    }

    fun getCallback(onSuccess: (bitmap: Bitmap) -> Unit): ActivityResultCallback<ActivityResult> {
        return ActivityResultCallback<ActivityResult> { result ->
            if (result.resultCode != Activity.RESULT_OK) return@ActivityResultCallback

            val activity = mContext.get() ?: return@ActivityResultCallback

            val imageUri = getPickImageResult(result.data) ?: return@ActivityResultCallback

            val cropImageView = CropImageView(activity)

            try {
                cropImageView.setImageBitmap(SecureMediaStore.getThumbnailFile(activity, imageUri, 512))

                // Use the Builder class for convenient dialog construction
                AlertDialog.Builder(activity)
                        .setView(cropImageView)
                        .setPositiveButton(R.string.ok) { _: DialogInterface?, _: Int -> cropImageView.croppedImage?.let { onSuccess(it) } }
                        .setNegativeButton(R.string.cancel, null)
                        .show()
            }
            catch (ioe: IOException) {
                Timber.e(ioe, "Couldn't load avatar")
            }
        }
    }
}