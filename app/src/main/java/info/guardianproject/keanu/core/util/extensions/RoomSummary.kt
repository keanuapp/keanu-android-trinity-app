package info.guardianproject.keanu.core.util.extensions

import info.guardianproject.keanu.core.ImApp
import org.matrix.android.sdk.api.session.room.model.RoomSummary
import org.matrix.android.sdk.api.session.room.model.tag.RoomTag

val RoomSummary.isArchived: Boolean
    get() = this.tags.find { it.name == RoomTag.ARCHIVED } != null

val RoomTag.Companion.ARCHIVED
    get() = "archived"

/**
 * It seems, there's currently an issue with #displayName. This is a workaround to fix that.
 * Deprecate and remove, as soon as #displayName does what it should again!
 */
val RoomSummary.displayNameWorkaround: String
    get() {
        if (name.isNotBlank()) return name

        val userId = if (isDirect) directUserId else otherMemberIds.firstOrNull()

        if (userId != null) {
            return ImApp.sImApp?.matrixSession?.userService()?.getUser(userId)?.displayName ?: displayName
        }

        return displayName
    }