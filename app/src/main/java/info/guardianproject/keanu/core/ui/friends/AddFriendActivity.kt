package info.guardianproject.keanu.core.ui.friends

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.provider.Telephony
import android.text.Editable
import android.text.TextWatcher
import android.view.*
import android.view.inputmethod.EditorInfo
import android.widget.TextView
import androidx.appcompat.widget.ListPopupWindow
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import info.guardianproject.keanu.core.ui.onboarding.OnboardingManager
import info.guardianproject.keanu.core.ui.onboarding.OnboardingManager.decodeInviteLink
import info.guardianproject.keanu.core.ui.onboarding.OnboardingManager.generateInviteMessage
import info.guardianproject.keanu.core.ui.onboarding.OnboardingManager.generateMatrixLink
import info.guardianproject.keanu.core.ui.onboarding.OnboardingManager.inviteNearby
import info.guardianproject.keanu.core.ui.onboarding.OnboardingManager.inviteSmsContact
import info.guardianproject.keanu.core.ui.onboarding.OnboardingManager.inviteScan
import info.guardianproject.keanu.core.ui.onboarding.OnboardingManager.inviteShare
import info.guardianproject.keanu.core.ui.onboarding.OnboardingManager.inviteShareToPackage
import info.guardianproject.keanuapp.R
import info.guardianproject.keanuapp.databinding.AddContactActivityBinding
import info.guardianproject.keanuapp.tasks.AddContactAsyncTask
import info.guardianproject.keanuapp.ui.BaseActivity
import info.guardianproject.keanuapp.ui.legacy.SimpleAlertHandler
import timber.log.Timber

class AddFriendActivity : BaseActivity() {

    companion object {
        const val EXTRA_ADD_LOCAL_FRIEND = "addLocalContact"
        const val EXTRA_USERNAME = "username"
        private const val MY_PERMISSIONS_REQUEST_CAMERA = 1
    }


    private lateinit var mBinding: AddContactActivityBinding

    var mHandler: SimpleAlertHandler? = null
    private var mAddLocalContact = true

    override fun onCreate(savedInstanceState: Bundle?) {
        supportRequestWindowFeature(Window.FEATURE_NO_TITLE)
        super.onCreate(savedInstanceState)

        title = ""

        mHandler = SimpleAlertHandler(this)

        mBinding = AddContactActivityBinding.inflate(layoutInflater)
        setContentView(mBinding.root)

        mBinding.inputContactLabel.text = getString(R.string.enter_your_friends_account, mApp.matrixSession?.myUserId)

        mBinding.email.addTextChangedListener(mTextWatcher)
        mBinding.email.setOnEditorActionListener { _: TextView?, actionId: Int, _: KeyEvent? ->
            if (actionId == EditorInfo.IME_ACTION_SEND) {
                addFriend()
            }

            false
        }

        setupActions()

        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        mAddLocalContact = intent.getBooleanExtra(EXTRA_ADD_LOCAL_FRIEND, true)

        if (intent.scheme == "matrix") {
            addContactFromUri(intent.data)
        }

        if (intent.hasExtra(EXTRA_USERNAME)) {
            mBinding.email.setText(intent.getStringExtra(EXTRA_USERNAME))
        }
    }

    private fun isPackageInstalled(packagename: String, packageManager: PackageManager): Boolean {
        return try {
            packageManager.getPackageInfo(packagename, 0)
            true
        } catch (e: PackageManager.NameNotFoundException) {
            false
        }
    }

    private val inviteMessage: String?
        get() {
            val userid = mApp.matrixSession?.myUserId ?: return null
            val nickname = mApp.matrixSession?.userService()?.getUser(userid)?.displayName ?: return null

            return generateInviteMessage(this@AddFriendActivity, nickname, userid)
        }

    private fun setupActions() {
        val pm = packageManager

        mBinding.btnAddFriend.setOnClickListener {
            if (mBinding.email.text?.isNotEmpty() == true) {
                addFriend()
            }
        }

        // WeChat installed?
        val packageNameWeChat = "com.tencent.mm"
        if (isPackageInstalled(packageNameWeChat, pm)) {
            try {
                val icon = packageManager.getApplicationIcon(packageNameWeChat)
                mBinding.btnInviteWeChat.setImageDrawable(icon)
            }
            catch (e: PackageManager.NameNotFoundException) {
                e.printStackTrace()
            }

            mBinding.btnInviteWeChat.setOnClickListener {
                inviteShareToPackage(
                    this@AddFriendActivity,
                    inviteMessage,
                    packageNameWeChat
                )
            }
        }
        else {
            mBinding.btnInviteWeChat.visibility = View.GONE
        }

        // WhatsApp installed?
        val packageNameWhatsApp = "com.whatsapp"
        if (isPackageInstalled(packageNameWhatsApp, pm)) {
            try {
                val icon = packageManager.getApplicationIcon(packageNameWhatsApp)
                mBinding.btnInviteWhatsApp.setImageDrawable(icon)
            }
            catch (e: PackageManager.NameNotFoundException) {
                e.printStackTrace()
            }

            mBinding.btnInviteWhatsApp.setOnClickListener {
                inviteShareToPackage(
                    this@AddFriendActivity,
                    inviteMessage,
                    packageNameWhatsApp
                )
            }
        }
        else {
            mBinding.btnInviteWhatsApp.visibility = View.GONE
        }

        // Populate SMS option
        if (pm.hasSystemFeature(PackageManager.FEATURE_TELEPHONY)) {
            val defaultSmsPackageName = Telephony.Sms.getDefaultSmsPackage(this)
            if (defaultSmsPackageName != null) {
                try {
                    val icon = packageManager.getApplicationIcon(defaultSmsPackageName)
                    mBinding.btnInviteSMS.setImageDrawable(icon)
                } catch (e: PackageManager.NameNotFoundException) {
                    e.printStackTrace()
                }
            }
        } else {
            mBinding.btnInviteSMS.visibility = View.GONE
        }

        mBinding.btnInviteSMS.setOnClickListener {
            inviteSmsContact(
                this@AddFriendActivity,
                null
            )
        }

        mBinding.btnInviteShare.setOnClickListener {
            inviteShare(
                this@AddFriendActivity,
                inviteMessage
            )
        }

        mBinding.btnInviteScan.setOnClickListener {
            if (hasCameraPermission()) {
                mApp.matrixSession?.myUserId?.let {
                    inviteScan(this@AddFriendActivity, generateMatrixLink(it))
                }
            }
        }
        mBinding.btnInviteNearby.setOnClickListener {
            mApp.matrixSession?.myUserId?.let {
                inviteNearby(this@AddFriendActivity, generateMatrixLink(it))
            }
        }
    }

    private fun hasCameraPermission(): Boolean {
        return if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_DENIED) {
            ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.CAMERA),
                MY_PERMISSIONS_REQUEST_CAMERA)

            false
        }
        else {
            true
        }
    }

    private var mFoundOne = false

    @Synchronized
    private fun addFriend() {
        if (mFoundOne) return

        val recipients = mBinding.email.text.toString().split(",")
        var addAddress: String? = null
        for (address: String in recipients) {
            addAddress = address
            if (addAddress.trim { it <= ' ' }.isNotEmpty()) {
                if (!address.startsWith("@")) {
                    addAddress = '@'.toString() + address + ':' + getString(R.string.default_server)
                }

                if (mAddLocalContact) {
                    AddContactAsyncTask().execute(addAddress)
                }

                mFoundOne = true
            }
        }

        if (mFoundOne) {
            val intent = Intent()
            intent.putExtra(FriendsPickerActivity.EXTRA_RESULT_USERNAME, addAddress)
            setResult(RESULT_OK, intent)
        }

        finish()
    }

    var mDomainList: ListPopupWindow? = null

    private val mTextWatcher: TextWatcher = object : TextWatcher {
        override fun afterTextChanged(s: Editable) {
            mDomainList?.dismiss()
        }

        override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {
            // noop
        }

        override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
            // noop
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, resultIntent: Intent?) {
        super.onActivityResult(requestCode, resultCode, resultIntent)
        if (resultCode == RESULT_OK) {
            if (requestCode == OnboardingManager.REQUEST_SCAN) {
                val resultScans = resultIntent!!.getStringArrayListExtra("result")
                for (resultScan: String in resultScans!!) {
                    Timber.v("Result--$resultScan")
                    try {
                        if (resultScan.startsWith("keanu://")) {
                            val params = Uri.parse(resultScan).getQueryParameters("id")
                            if (params.size > 0) {
                                val address = params[0]
                                if (mAddLocalContact) AddContactAsyncTask().execute(address, null)
                                val intent = Intent()
                                intent.putExtra(
                                    FriendsPickerActivity.EXTRA_RESULT_USERNAME,
                                    address
                                )
                                Timber.v("Result 1--$address")
                                setResult(RESULT_OK, intent)
                            }
                        } else {
                            //parse each string and if they are for a new user then add the user
                            val diLink = decodeInviteLink(resultScan)
                            if (mAddLocalContact) AddContactAsyncTask().execute(
                                diLink!!.username,
                                diLink.fingerprint,
                                diLink.nickname
                            )
                            val intent = Intent()
                            intent.putExtra(
                                FriendsPickerActivity.EXTRA_RESULT_USERNAME,
                                diLink!!.username
                            )
                            Timber.v("Result 2--%s", diLink.username)
                            setResult(RESULT_OK, intent)
                        }

                        //if they are for a group chat, then add the group
                    } catch (e: Exception) {
                        Timber.v("Result 3--%s", e.message)
                        Timber.w(e, "error parsing QR invite link")
                    }
                }
            }
            finish()
        }
    }

    /**
     * Implement `xmpp:` URI parsing according to the RFC: http://tools.ietf.org/html/rfc5122
     * @param uri the URI to be parsed
     */
    private fun addContactFromUri(uri: Uri?) {
        Timber.i("addContactFromUri: %s scheme: %s", uri, uri?.scheme)
        mBinding.email.setText(uri?.getQueryParameter("id"))
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                finish()
                return true
            }
        }

        return super.onOptionsItemSelected(item)
    }
}