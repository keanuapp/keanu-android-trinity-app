package info.guardianproject.keanu.core.ui.me.settings

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import info.guardianproject.keanu.core.ui.me.providers.PreferenceProvider
import info.guardianproject.keanuapp.R
import info.guardianproject.keanuapp.databinding.FragmentSettingSecurityPrivacyBinding

class SettingSecurityPrivacyFragment : Fragment() {

    private lateinit var mBinding: FragmentSettingSecurityPrivacyBinding
    private lateinit var preferenceProvider: PreferenceProvider

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        mBinding = FragmentSettingSecurityPrivacyBinding.inflate(inflater, container, false)

        (activity as SettingsActivity).setBackButtonText(getString(R.string.profile_security_privacy))
        preferenceProvider = PreferenceProvider(requireActivity().applicationContext)
        mBinding.sbBlockScreenshots.isChecked = preferenceProvider.getScreenshotInfo()
        mBinding.sbBlockScreenshots.setOnCheckedChangeListener { _, isChecked ->
            preferenceProvider.setScreenshotInfo(isChecked)
        }

        return mBinding.root
    }


}