package info.guardianproject.keanu.core.util.extensions

import org.matrix.android.sdk.api.failure.Failure

val Throwable.betterMessage: String
    get() {
        var msg = when (this) {
            is Failure.ServerError -> {
                error.message
            }
            is Exception -> {
                // If we don't cast, Throwable#localizedMessage will return nothing.
                localizedMessage
            }
            else -> {
                localizedMessage
            }
        }

        if (msg.isNullOrBlank()) msg = message
        if (msg.isNullOrBlank()) msg = toString()

        return msg
    }