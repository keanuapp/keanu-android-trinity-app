package info.guardianproject.keanu.core.util.constants

object BundleKeys {
    const val BUNDLE_KEY_SETTING_SCREEN = "bundle_key_setting_screen"
    const val BUNDLE_KEY_DISPLAY_NAME = "bundle_key_display_name"
    const val BUNDLE_KEY_IMAGE_NAME = "bundle_key_image_name"
}