package info.guardianproject.keanu.core.ui.quickresponse

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.vanniktech.emoji.EmojiTextView
import info.guardianproject.keanuapp.R

class EmojiAdapter(
    private val onItemClick: (position: Int) -> Unit
) :
    RecyclerView.Adapter<EmojiAdapter.EmojiViewHolder>() {
    var recentEmojiList: List<String>? = null
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): EmojiViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.recent_emoji_list_item, parent, false)

        return EmojiViewHolder(view, onItemClick)
    }

    override fun onBindViewHolder(holder: EmojiViewHolder, position: Int) {
        val selectedEmoji = recentEmojiList?.get(position)
        holder.emojiTextView.setEmojiSizeRes(R.dimen.emoji_size)
        holder.emojiTextView.text = selectedEmoji
    }

    override fun getItemCount(): Int {
        return recentEmojiList?.size ?: 0
    }

    class EmojiViewHolder(view: View, private val onItemClicked: (position: Int) -> Unit) :
        RecyclerView.ViewHolder(view), View.OnClickListener {
        val emojiTextView: EmojiTextView = view.findViewById(R.id.emojiTextView)

        init {
            view.setOnClickListener(this)
        }

        override fun onClick(v: View?) {
            val position = absoluteAdapterPosition
            onItemClicked(position)
        }


    }

}