package info.guardianproject.keanu.core.verification

import android.annotation.SuppressLint
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.os.Bundle
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.widget.Button
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.content.ContextCompat
import info.guardianproject.keanu.core.util.BitmapHelper
import info.guardianproject.keanu.core.util.extensions.betterMessage
import info.guardianproject.keanuapp.R
import info.guardianproject.keanuapp.databinding.ActivityVerificationBinding
import info.guardianproject.keanuapp.ui.BaseActivity
import org.matrix.android.sdk.api.session.Session
import org.matrix.android.sdk.api.session.crypto.CryptoService
import org.matrix.android.sdk.api.session.crypto.verification.*

@SuppressLint("LogNotTimber")
class VerificationActivity : BaseActivity(), View.OnClickListener, VerificationService.Listener {

    sealed class State {
        data class IncomingRequest(val otherUserId: String, val otherDeviceId: String?) : State()

        object WaitingOnAccept : State()

        data class ChooseMethod(val canSas: Boolean, val canScan: Boolean, val qrCode: String?) : State()

        object WaitingOnChallenge : State()

        data class ShowSas(val emojis: String) : State()

        object QrScannedByOther : State()

        object WaitingOnConfirm : State()

        data class Verified(val otherUserId: String, val otherDeviceId: String?) : State()

        data class Cancelled(val reason: String?, val byMe: Boolean) : State()

        data class Error(val failure: Throwable? = null) : State()
    }

    enum class ActionStyle {
        Positive, Negative, Neutral, LaidBack
    }

    private lateinit var mBinding: ActivityVerificationBinding

    private var mGreen = 0
    private var mRed = 0
    private var mBlue = 0
    private var mWhite = 0
    private var mTransparent = 0

    override var mSession: Session? = null
        set(value) {
            field?.cryptoService()?.verificationService()?.removeListener(this)

            field = value

            field?.cryptoService()?.verificationService()?.addListener(this)
        }

    private val mCryptoService: CryptoService?
        get() = mSession?.cryptoService()

    private val mVerificationService: VerificationService?
        get() = mCryptoService?.verificationService()

    private val mCanScan: Boolean
        get() = packageManager.hasSystemFeature(PackageManager.FEATURE_CAMERA_ANY)

    private var mTransaction: VerificationTransaction?
        get() = VerificationManager.instance.transaction
        set(value) {
            VerificationManager.instance.transaction = value
        }

    private var mRequest: PendingVerificationRequest?
        get() = VerificationManager.instance.request
        set(value) {
            VerificationManager.instance.request = value
        }

    private var mState: State
        get() = VerificationManager.instance.state
        set(value) {
            VerificationManager.instance.state = value
        }

    private val mGetQrCode = registerForActivityResult(ActivityResultContracts.StartActivityForResult()) {
        if (it.resultCode == RESULT_OK) {
            val result = it.data?.getStringArrayListExtra("result")?.first() ?: return@registerForActivityResult

            (mTransaction as? QrCodeVerificationTransaction)?.userHasScannedOtherQrCode(result)

            return@registerForActivityResult
        }
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        mBinding = ActivityVerificationBinding.inflate(layoutInflater)
        setContentView(mBinding.root)

        mBinding.btAction1.setOnClickListener(this)
        mBinding.btAction2.setOnClickListener(this)

        mGreen = ContextCompat.getColor(this, android.R.color.holo_green_dark)
        mRed = ContextCompat.getColor(this, android.R.color.holo_red_dark)
        mBlue = ContextCompat.getColor(this, android.R.color.holo_blue_dark)
        mWhite = ContextCompat.getColor(this, android.R.color.white)
        mTransparent = ContextCompat.getColor(this, android.R.color.transparent)

        supportActionBar?.title = getString(R.string.Verify_Device)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        mSession = mApp.matrixSession

        // If one of these is set on start, that means it was set by VerificationManager,
        // which probably means somebody asked us for verification.
        when {
            mTransaction != null && mTransaction?.state != VerificationTxState.None -> {
                render(mTransaction!!)
            }
            mRequest != null -> {
                render(mRequest!!)
            }
            else -> {
                render()
            }
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            done()

            return true
        }

        return super.onOptionsItemSelected(item)
    }

    override fun onBackPressed() {
        done()

        super.onBackPressed()
    }

    private fun render(transaction: VerificationTransaction) {
        Log.d(this::class.java.canonicalName, "#render transaction=${transaction}")

        mTransaction = transaction

        when (transaction.state) {
            is VerificationTxState.VerificationSasTxState -> {
                when (transaction) {
                    is IncomingSasVerificationTransaction -> {
                        when (transaction.uxState) {
                            IncomingSasVerificationTransaction.UxState.SHOW_ACCEPT -> {
                                mState = State.IncomingRequest(transaction.otherUserId, transaction.otherDeviceId)
                            }

                            IncomingSasVerificationTransaction.UxState.WAIT_FOR_KEY_AGREEMENT -> {
                                mState = State.WaitingOnChallenge
                            }

                            IncomingSasVerificationTransaction.UxState.SHOW_SAS -> {
                                mState = State.ShowSas(transaction.getEmojiCodeRepresentation().joinToString("\n\n") { "${it.emoji} ${getString(it.nameResId)}" })
                            }

                            IncomingSasVerificationTransaction.UxState.WAIT_FOR_VERIFICATION -> {
                                mState = State.WaitingOnConfirm
                            }

                            else -> {
                                mState = State.WaitingOnAccept
                            }
                        }
                    }
                    is OutgoingSasVerificationTransaction -> {
                        when (transaction.uxState) {
                            OutgoingSasVerificationTransaction.UxState.WAIT_FOR_KEY_AGREEMENT -> {
                                mState = State.WaitingOnChallenge
                            }

                            OutgoingSasVerificationTransaction.UxState.SHOW_SAS -> {
                                mState = State.ShowSas(transaction.getEmojiCodeRepresentation().joinToString("\n\n") { "${it.emoji} ${getString(it.nameResId)}" })
                            }

                            OutgoingSasVerificationTransaction.UxState.WAIT_FOR_VERIFICATION -> {
                                mState = State.WaitingOnConfirm
                            }

                            else -> {
                                mState = State.WaitingOnAccept
                            }
                        }
                    }
                    else -> {
                        mState = State.Error()
                    }
                }
            }

            is VerificationTxState.QrScannedByOther -> {
                mState = State.QrScannedByOther
            }

            is VerificationTxState.WaitingOtherReciprocateConfirm -> {
                mState = State.WaitingOnConfirm
            }

            is VerificationTxState.Verified -> {
                mState = State.Verified(transaction.otherUserId, transaction.otherDeviceId)
            }

            is VerificationTxState.Cancelled -> {
                mState = State.Cancelled(
                        (transaction.state as VerificationTxState.Cancelled).cancelCode.humanReadable,
                        (transaction.state as VerificationTxState.Cancelled).byMe)
            }

            else -> {
                mState = State.WaitingOnAccept
            }
        }

        render()
    }

    private fun render(request: PendingVerificationRequest): VerificationActivity {
        Log.d(this::class.java.canonicalName, "#render request=${request}")

        mRequest = request

        when {
            request.isSuccessful -> {
                mState = State.Verified(request.otherUserId, request.requestInfo?.fromDevice ?: request.readyInfo?.fromDevice)
            }

            request.isFinished -> {
                mState = State.Cancelled(request.cancelConclusion?.humanReadable, false)
            }

            request.isReady -> {
                val methods = request.readyInfo?.methods

                if (methods?.isEmpty() != false) {
                    mState = State.Error(RuntimeException(getString(R.string.No_mutually_supported_verification_methods_)))
                }
                else {
                    if (methods.size == 1 && request.isSasSupported()) {
                        mState = State.WaitingOnChallenge

                        if (mRequest != null && mRequest?.otherUserId == mSession?.myUserId) {
                            startSas(mRequest!!)
                        }
                    } else {
                        mState = State.ChooseMethod(request.isSasSupported(), mCanScan,
                                if (request.otherCanScanQrCode()) (mTransaction as? QrCodeVerificationTransaction)?.qrCodeText else null)
                    }
                }
            }

            request.isIncoming -> {
                mState = State.IncomingRequest(request.otherUserId, request.requestInfo?.fromDevice
                        ?: request.readyInfo?.fromDevice)
            }

            else -> {
                mState = State.WaitingOnChallenge
            }
        }

        return render()
    }

    private fun render(state: State? = null): VerificationActivity {
        if (state != null) mState = state

        Log.d(this::class.java.canonicalName, "#render state=${mState}")

        var title: String? = null
        var subtitle: String? = null
        var message: String? = null
        var image: Bitmap? = null
        var showGreenShield = false
        var action1: String? = null
        var actionStyle1 = ActionStyle.LaidBack
        var action2: String? = null
        var actionStyle2 = ActionStyle.LaidBack

        when (mState) {
            is State.IncomingRequest -> {
                val otherUserId = (mState as State.IncomingRequest).otherUserId
                title = getString(R.string._wants_to_verify, mSession?.userService()?.getUser(otherUserId)?.displayName ?: otherUserId)
                subtitle = otherUserId
                action1 = getString(R.string.Accept)
                actionStyle1 = ActionStyle.Positive
                action2 = getString(R.string.Decline)
                actionStyle2 = ActionStyle.Negative
            }

            is State.WaitingOnAccept -> {
                title = getString(R.string.Waiting)
                subtitle = getString(R.string.Waiting_on_other_party_to_accept_)
            }

            is State.ChooseMethod -> {
                title = getString(R.string.Choose_Method)

                val s = mState as State.ChooseMethod

                if (s.canSas && s.canScan && s.qrCode != null) {
                    subtitle = getString(R.string.Show_the_other_party_this_QR_code_scan_theirs_or_compare_emoji_instead_)
                }
                else if (s.canSas && s.canScan && s.qrCode == null) {
                    subtitle = getString(R.string.Scan_the_other_partys_QR_code_or_compare_emoji_instead_)
                }
                else if (!s.canSas && s.canScan && s.qrCode != null) {
                    subtitle = getString(R.string.Show_the_other_party_this_QR_code_or_scan_theirs_)
                }
                else if (s.canSas && !s.canScan && s.qrCode != null) {
                    subtitle = getString(R.string.Show_the_other_party_this_QR_code_or_compare_emoji_instead_)
                }

                // The rest should never happen.

                if (s.qrCode != null) {
                    image = BitmapHelper.qrCode(s.qrCode, 128, 128)
                }

                if (s.canScan) {
                    action1 = getString(R.string.Scan_QR_Code)
                }

                if (s.canSas) {
                    action2 = getString(R.string.Compare_Emoji)
                }
            }

            is State.WaitingOnChallenge -> {
                title = getString(R.string.Waiting)
                subtitle = getString(R.string.Waiting_on_challenge_negotiation_)
            }

            is State.ShowSas -> {
                title = getString(R.string.Compare_Emoji)
                subtitle = getString(R.string.Confirm_the_emoji_below_are_displayed_on_both_sessions_in_the_same_order_)
                message = (mState as State.ShowSas).emojis
                action1 = getString(R.string.They_Dont_Match)
                actionStyle1 = ActionStyle.Negative
                action2 = getString(R.string.They_Match)
                actionStyle2 = ActionStyle.Positive
            }

            is State.QrScannedByOther -> {
                title = getString(R.string.Scanned)
                subtitle = getString(R.string.Is_the_other_device_showing_the_same_shield_)
                showGreenShield = true
                action1 = getString(R.string.No)
                actionStyle1 = ActionStyle.Negative
                action2 = getString(R.string.Yes)
                actionStyle2 = ActionStyle.Positive
            }

            is State.WaitingOnConfirm -> {
                title = getString(R.string.Verification_Successful_)
                subtitle = getString(R.string.Waiting_on_confirmation_from_other_device_)
                showGreenShield = true
            }

            is State.Verified -> {
                title = getString(R.string.Verification_Successful_)

                val s = mState as State.Verified

                val deviceName = mCryptoService?.getCryptoDeviceInfo(s.otherUserId, s.otherDeviceId)?.displayName()
                        ?: s.otherDeviceId

                subtitle = if (s.otherUserId == mSession?.myUserId) {
                    getString(R.string._is_now_a_trusted_device_, deviceName)
                } else {
                    val userName = mSession?.userService()?.getUser(s.otherUserId)?.displayName

                    getString(R.string._of_is_now_a_trusted_device_, deviceName, userName)
                }

                showGreenShield = true
                action1 = getString(R.string.Done)
                actionStyle1 = ActionStyle.Neutral

            }
            is State.Cancelled -> {
                val s = mState as State.Cancelled

                title = getString(R.string.Cancelled)
                subtitle = if (s.byMe) getString(R.string.Request_was_cancelled) else getString(R.string.The_other_party_cancelled_the_request)
                message = getString(R.string.Reason_, s.reason ?: getString(R.string.No_reason_available_))
                action1 = getString(android.R.string.ok)
                actionStyle1 = ActionStyle.Neutral
            }

            is State.Error -> {
                title = getString(R.string.Error)
                message = (mState as State.Error).failure?.betterMessage ?: getString(R.string.No_description_available)
                action1 = getString(android.R.string.ok)
                actionStyle1 = ActionStyle.Neutral
            }
        }

        mBinding.tvTitle.text = title

        mBinding.tvSubtitle.text = subtitle
        mBinding.tvSubtitle.visibility = if (subtitle == null) View.GONE else View.VISIBLE

        mBinding.tvMessage.text = message
        mBinding.tvMessage.visibility = if (message == null) View.GONE else View.VISIBLE

        if (showGreenShield) {
            mBinding.image.setImageResource(R.drawable.ic_verified_user)
            mBinding.image.setColorFilter(mGreen)
            mBinding.image.layoutParams.height = (128 * resources.displayMetrics.density).toInt()
        }
        else {
            mBinding.image.clearColorFilter()
            mBinding.image.layoutParams.height = (256 * resources.displayMetrics.density).toInt()
        }

        if (image != null) mBinding.image.setImageBitmap(image)

        mBinding.image.visibility = if (image == null && !showGreenShield) View.GONE else View.VISIBLE

        setAction(mBinding.btAction1, action1, actionStyle1)

        setAction(mBinding.btAction2, action2, actionStyle2)

        Log.d(this::class.java.canonicalName, "#rendered state=${mState}")

        return this
    }

    override fun onClick(v: View?) {
        when (v) {
            mBinding.btAction1 -> {
                when (mState) {
                    is State.IncomingRequest -> {
                        val request = mRequest
                        val service = mVerificationService
                        val tid = request?.transactionId ?: request?.requestInfo?.transactionId ?: request?.readyInfo?.transactionId
                        val transaction = mTransaction as? IncomingSasVerificationTransaction

                        if (request != null && service != null && tid != null) {
                            render(State.WaitingOnAccept)

                            val methods = arrayListOf(VerificationMethod.QR_CODE_SHOW, VerificationMethod.SAS)
                            if (mCanScan) methods.add(VerificationMethod.QR_CODE_SCAN)

                            val roomId = request.roomId

                            if (roomId != null) {
                                service.readyPendingVerificationInDMs(methods, request.otherUserId,
                                    roomId, tid)
                            }
                            else {
                                service.readyPendingVerification(methods, request.otherUserId, tid)
                            }
                        }
                        else if (transaction != null) {
                            transaction.performAccept()
                        }
                        else {
                            render(State.Error())
                        }
                    }

                    is State.ChooseMethod -> {
                        if ((mState as State.ChooseMethod).canScan) {
                            mGetQrCode.launch(mApp.router.qrScan(this, finishOnFirst = true, getRaw = true))
                        }
                    }

                    is State.ShowSas -> {
                        if (mRequest?.otherUserId ?: mTransaction?.otherUserId == mSession?.myUserId) {
                            startActivity(mApp.router.intrusion(this))
                        }

                        cancel()
                    }

                    is State.QrScannedByOther -> {
                        (mTransaction as? QrCodeVerificationTransaction)?.otherUserDidNotScannedMyQrCode()
                    }

                    is State.Verified -> done()

                    else -> cancel()
                }
            }

            mBinding.btAction2 -> {
                when (mState) {
                    is State.IncomingRequest -> cancel()

                    is State.ChooseMethod -> {
                        if ((mState as State.ChooseMethod).canSas) {
                            var request = mRequest
                            val transaction = mTransaction

                            if (request == null && transaction != null) {
                                request = mVerificationService?.getExistingVerificationRequest(transaction.otherUserId, transaction.transactionId)
                            }
                            
                            if (request != null) startSas(request)
                        }
                    }

                    is State.ShowSas -> {
                        (mTransaction as? SasVerificationTransaction)?.userHasVerifiedShortCode()
                    }

                    is State.QrScannedByOther -> {
                        (mTransaction as? QrCodeVerificationTransaction)?.otherUserScannedMyQrCode()
                    }

                    else -> cancel()
                }
            }
        }
    }

    override fun verificationRequestCreated(pr: PendingVerificationRequest) {
        render(pr)
    }

    override fun verificationRequestUpdated(pr: PendingVerificationRequest) {
        render(pr)
    }

    override fun transactionCreated(tx: VerificationTransaction) {
        render(tx)
    }

    override fun transactionUpdated(tx: VerificationTransaction) {
        render(tx)
    }

    private fun cancel() {
        mRequest?.let {
            mVerificationService?.cancelVerificationRequest(it)
        }

        mTransaction?.cancel(CancelCode.User)

        done()
    }

    private fun done() {
        finish()

        mRequest = null
        mTransaction = null
        mSession = null
    }

    private fun startSas(request: PendingVerificationRequest) {
        val info = request.readyInfo

        if (info != null) {
            mVerificationService?.beginKeyVerification(VerificationMethod.SAS, request.otherUserId,
                    info.fromDevice, info.transactionId)
        }
        else {
            render(State.Error(RuntimeException("Request wasn't ready to start SAS!")))
        }
    }

    private fun setAction(button: Button, text: String?, style: ActionStyle) {
        if (text == null) {
            button.visibility = View.GONE
            return
        }

        when (style) {
            ActionStyle.Positive -> {
                button.setBackgroundColor(mGreen)
                button.setTextColor(mWhite)
            }

            ActionStyle.Negative -> {
                button.setBackgroundColor(mRed)
                button.setTextColor(mWhite)
            }

            ActionStyle.Neutral -> {
                button.setBackgroundColor(mBlue)
                button.setTextColor(mWhite)
            }

            else -> {
                button.setBackgroundColor(mTransparent)
                button.setTextColor(mBlue)
            }
        }

        button.text = text
        button.visibility = View.VISIBLE
    }
}