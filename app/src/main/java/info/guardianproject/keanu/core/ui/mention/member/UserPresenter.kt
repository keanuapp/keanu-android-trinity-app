package info.guardianproject.keanu.core.ui.mention.member

import android.content.Context
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.otaliastudios.autocomplete.RecyclerViewPresenter
import org.matrix.android.sdk.api.query.QueryStringValue
import org.matrix.android.sdk.api.session.room.Room
import org.matrix.android.sdk.api.session.room.members.roomMemberQueryParams
import org.matrix.android.sdk.api.session.room.model.Membership
import org.matrix.android.sdk.api.session.room.model.RoomMemberSummary

class UserPresenter(context: Context, val room: Room) :
    RecyclerViewPresenter<RoomMemberSummary>(context) {
    lateinit var adapter: MentionUserAdapter
    private var mUserClickListener = object : UserClickListener {
        override fun onUserClicked(user: RoomMemberSummary) {
            dispatchClick(user)
        }
    }

    override fun getPopupDimensions(): PopupDimensions {
        val dims = PopupDimensions()
        dims.width = ViewGroup.LayoutParams.WRAP_CONTENT
        dims.height = ViewGroup.LayoutParams.WRAP_CONTENT
        return dims
    }

    override fun instantiateAdapter(): RecyclerView.Adapter<*> {
        adapter = MentionUserAdapter(context)
        adapter.setUserClickListener(mUserClickListener)
        return adapter
    }

    override fun onQuery(query: CharSequence?) {

        val queryParams = roomMemberQueryParams {
            displayName = if (query.isNullOrBlank()) {
                QueryStringValue.IsNotEmpty
            } else {
                QueryStringValue.Contains(query.toString(), QueryStringValue.Case.INSENSITIVE)
            }
            memberships = listOf(Membership.JOIN)
            excludeSelf = true
        }
        val members = room.membershipService().getRoomMembers(queryParams)
            .asSequence()
            .sortedBy { it.displayName }
            .disambiguate()
            .toList()

        adapter.setData(members)
        adapter.notifyDataSetChanged()

    }

    interface UserClickListener {
        fun onUserClicked(user: RoomMemberSummary)
    }
}

private fun Sequence<RoomMemberSummary>.disambiguate(): Sequence<RoomMemberSummary> {
    val displayNames = hashMapOf<String, Int>().also { map ->
        for (item in this) {
            item.displayName?.lowercase()?.also { displayName ->
                map[displayName] = map.getOrPut(displayName) { 0 } + 1
            }
        }
    }

    return map { roomMemberSummary ->
        if ((displayNames[roomMemberSummary.displayName?.lowercase()] ?: 0) > 1) {
            roomMemberSummary.copy(displayName = roomMemberSummary.displayName + " " + roomMemberSummary.userId)
        } else roomMemberSummary
    }
}