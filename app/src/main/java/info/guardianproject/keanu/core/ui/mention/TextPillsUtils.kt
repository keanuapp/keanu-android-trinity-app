package info.guardianproject.keanu.core.ui.mention

import android.text.SpannableString
import org.matrix.android.sdk.api.session.room.model.RoomMemberSummary
import org.matrix.android.sdk.api.session.room.send.MatrixItemSpan
import java.util.*

class TextPillsUtils {

    companion object {
        const val MENTION_TAG = "a"
        private const val MENTION_SPAN_TO_HTML_TEMPLATE =
            "<a href=\"https://matrix.to/#/%1\$s\">%2\$s</a>"

        /**
         * Detects if transformable spans are present in the text.
         * @return the transformed String or null if no Span found
         */
        fun processSpecialSpansToHtml(
            text: CharSequence,
            users: Map<String?, RoomMemberSummary>
        ): String {
            return getMentionedString(text, users, MENTION_SPAN_TO_HTML_TEMPLATE)
        }

        private fun getMentionedString(
            text: CharSequence,
            users: Map<String?, RoomMemberSummary>,
            template: String
        ): String {
            val builder = StringBuilder()
            text.split(" ").map { content ->
                val formattedName = content.replace(":", "")
                if (users.containsKey(formattedName)) {
                    val selectedUser = users[formattedName]
                    val mentionTag = String.format(
                        template,
                        selectedUser?.userId,
                        selectedUser?.displayName
                    )
                    builder.append(" ")
                    builder.append(mentionTag)
                } else {
                    builder.append(" ")
                    builder.append(formattedName)
                }


            }
            return builder.toString()
        }

        private fun transformPills(text: CharSequence, template: String): String? {
            val spannableString = SpannableString.valueOf(text)
            val pills = spannableString
                ?.getSpans(0, text.length, MatrixItemSpan::class.java)
                ?.map {
                    MentionLinkSpec(
                        it,
                        spannableString.getSpanStart(it),
                        spannableString.getSpanEnd(it)
                    )
                }
                ?.toMutableList()
                ?.takeIf { it.isNotEmpty() }
                ?: return null

            // we need to prune overlaps!
            pruneOverlaps(pills)

            return buildString {
                var currIndex = 0
                pills.forEachIndexed { _, (urlSpan, start, end) ->
                    // We want to replace with the pill with a html link
                    // append text before pill
                    append(text, currIndex, start)
                    // append the pill

                    val formattedText = String.format(
                        template,
                        urlSpan.matrixItem.id,
                        urlSpan.matrixItem.displayName
                    )
                    append(formattedText)
                    currIndex = end
                }
                // append text after the last pill
                append(text, currIndex, text.length)
            }
        }

        private fun pruneOverlaps(links: MutableList<MentionLinkSpec>) {
            Collections.sort(links, MentionLinkSpecComparator())
            var len = links.size
            var i = 0
            while (i < len - 1) {
                val a = links[i]
                val b = links[i + 1]
                var remove = -1

                // test if there is an overlap
                if (b.start in a.start until a.end) {
                    when {
                        b.end <= a.end ->
                            // b is inside a -> b should be removed
                            remove = i + 1
                        a.end - a.start > b.end - b.start ->
                            // overlap and a is bigger -> b should be removed
                            remove = i + 1
                        a.end - a.start < b.end - b.start ->
                            // overlap and a is smaller -> a should be removed
                            remove = i
                    }

                    if (remove != -1) {
                        links.removeAt(remove)
                        len--
                        continue
                    }
                }
                i++
            }
        }
    }
}