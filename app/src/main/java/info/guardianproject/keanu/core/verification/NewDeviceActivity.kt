package info.guardianproject.keanu.core.verification

import android.os.Bundle
import android.view.MenuItem
import android.view.View
import info.guardianproject.keanuapp.R
import info.guardianproject.keanuapp.databinding.ActivityNewDeviceBinding
import info.guardianproject.keanuapp.ui.BaseActivity
import org.matrix.android.sdk.api.session.crypto.model.CryptoDeviceInfo

class NewDeviceActivity: BaseActivity(), View.OnClickListener {

    private lateinit var mBinding: ActivityNewDeviceBinding

    private var mDevice: CryptoDeviceInfo? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val userId = intent.getStringExtra(VerificationManager.EXTRA_USER_ID)
        val deviceId = intent.getStringExtra(VerificationManager.EXTRA_DEVICE_ID)

        userId?.let {
            mDevice = mSession?.cryptoService()?.getCryptoDeviceInfo(userId, deviceId)
        }

        mBinding = ActivityNewDeviceBinding.inflate(layoutInflater)
        setContentView(mBinding.root)

        mBinding.btNo.setOnClickListener(this)
        mBinding.btYes.setOnClickListener(this)

        mBinding.tvDeviceName.text = getString(R.string.Name_, mDevice?.displayName() ?: "–")

        mBinding.tvDeviceId.text = getString(R.string.Identifier_, deviceId ?: "–")

        supportActionBar?.title = getString(R.string.New_Log_In)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            finish()

            return true
        }

        return super.onOptionsItemSelected(item)
    }

    override fun onClick(v: View?) {
        if (v == mBinding.btYes) {
            mDevice?.let {
                VerificationManager.instance.interactiveVerify(this, it) {
                    finish()
                }
            } ?: finish()
        }
        else {
            startActivity(mApp.router.intrusion(this))

            finish()
        }
    }
}