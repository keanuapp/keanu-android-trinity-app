package info.guardianproject.keanu.core.service

import android.app.Notification
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.media.AudioManager
import android.os.Build
import android.os.VibrationEffect
import android.os.Vibrator
import android.provider.Settings
import androidx.core.app.NotificationCompat
import info.guardianproject.keanu.core.ImApp
import info.guardianproject.keanu.core.KeanuConstants
import info.guardianproject.keanu.core.util.extensions.displayNameWorkaround
import info.guardianproject.keanu.core.util.extensions.getSummary
import info.guardianproject.keanuapp.R
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import org.matrix.android.sdk.api.session.crypto.model.CryptoDeviceInfo
import org.matrix.android.sdk.api.session.events.model.EventType
import org.matrix.android.sdk.api.session.events.model.toModel
import org.matrix.android.sdk.api.session.room.model.relation.ReactionContent
import org.matrix.android.sdk.api.session.room.notification.RoomNotificationState
import org.matrix.android.sdk.api.session.room.timeline.TimelineEvent
import org.matrix.android.sdk.api.session.room.timeline.getEditedEventId
import timber.log.Timber

class StatusBarNotifier(private val mContext: Context) {

    companion object {
        val VIBRATE_PATTERN = longArrayOf(0, 250, 250, 250)

        private val VIBRATION_TIMINGS = longArrayOf(0, 500, 100, 100, 1000)

        private var UNIQUE_INT_PER_CALL = 10000

        private val ALLOWED_NOTIFICATION_TYPES = listOf(
            EventType.MESSAGE,
            EventType.STICKER,
            EventType.REACTION,
            EventType.REDACTION
        )
    }

    private val mCoroutineScope: CoroutineScope by lazy {
        CoroutineScope(Dispatchers.IO)
    }

    private val mApp
        get() = ImApp.sImApp

    private val mSession
        get() = mApp?.matrixSession

    private val mNotificationManager: NotificationManager? = mContext
        .getSystemService(Context.NOTIFICATION_SERVICE) as? NotificationManager

    private val mNotificationInfos = ArrayList<NotificationInfo>()

    private var mVibrator: Vibrator? = null

    private var mVibeEffect: VibrationEffect? = null

    fun notifyChat(te: TimelineEvent) {
        // Ignore, when type is not whitelisted.
        val clearType = te.root.getClearType()
        if (!ALLOWED_NOTIFICATION_TYPES.contains(clearType)) return

        val roomId = te.roomId

        // This will only be a REDACTION type, if the message was actually deleted.
        // In that case, we remove the old message for that event ID.
        // If it was a change, the type will be of the original event (e.g. MESSAGE)
        // and, since we're using the roomId as notification ID, will automatically be
        // updated later on.
        if (clearType == EventType.REDACTION) {
            val notification = mNotificationInfos.firstOrNull { it.id == roomId } ?: return

            // To be honest: The fallbacks are probably wrong, but better than nothing, maybe?
            notification.removeMessage(te.root.redacts ?: te.getEditedEventId() ?: te.eventId)

            // If that was the last message, cancel the whole notification.
            if (notification.isEmpty()) {
                return cancelNotification(roomId)
            }

            // Update the changed notification.
            return notify(notification)
        }

        // Ignore when already notified.
        if (mNotificationInfos.any { it.containsMessage(te.eventId) }) return

        if (mApp?.preferenceProvider?.isNotificationEnable() == false) {
            Timber.d("Notification for room $te.roomId is not enabled.")
            return cancelNotification(roomId)
        }

        // Ignore when room in foreground.
        if (mApp?.isRoomInForeground(roomId) == true) {
            return cancelNotification(roomId)
        }

        // Ignore, when our user already read this.
        if (te.readReceipts.firstOrNull { it.roomMember.userId == mSession?.myUserId } != null) {
            return cancelNotification(roomId)
        }

        val roomSummary = mSession?.roomService()?.getRoomSummary(roomId) ?: return cancelNotification(roomId)

        val msg = te.getSummary(mContext)

        val reaction = te.root.getClearContent()?.toModel<ReactionContent>()

        if (reaction?.relatesTo != null) {
            // This is a reaction.
            mCoroutineScope.launch {
                val parentEvent = reaction.relatesTo?.eventId?.let { mSession?.eventService()?.getEvent(te.roomId, it) }

                if (parentEvent?.senderId?.equals(mSession?.myUserId) == true) {
                    //if the sender is me, then show the reaction
                    val intent = getDefaultIntent(roomId)
                    intent?.action = Intent.ACTION_VIEW
                    intent?.addCategory(KeanuConstants.IMPS_CATEGORY)

                    notify(
                        roomId,
                        roomSummary.displayNameWorkaround,
                        te.getEditedEventId() ?: te.eventId,
                        msg,
                        intent,
                        R.drawable.ic_discuss
                    )
                }
            }
        } else {
            val intent = getDefaultIntent(roomId)
            intent?.action = Intent.ACTION_VIEW
            intent?.addCategory(KeanuConstants.IMPS_CATEGORY)

            notify(
                roomId, roomSummary.displayNameWorkaround, te.getEditedEventId() ?: te.eventId,
                msg, intent, R.drawable.ic_discuss
            )
        }
    }

    fun notifyNewDevice(device: CryptoDeviceInfo) {
        // Don't notify multiple times about the same device!
        if (mNotificationInfos.any { it.id == device.deviceId }) return

        val intent = mApp?.router?.newDevice(mContext, device.userId, device.deviceId)

        val title = mContext.getString(R.string.app_name)
        val message = mContext.getString(
            R.string._logged_into_your_account_Please_confirm_it_was_you_,
            device.displayName() ?: device.deviceId
        )

        notify(device.deviceId, title, device.deviceId, message, intent, R.drawable.ic_devices)
    }

    fun cancelNotification(id: String) {
        val notification = mNotificationInfos.firstOrNull { it.id == id } ?: return

        mNotificationManager?.cancel(notification.idInt)
        mNotificationInfos.remove(notification)
    }

    private fun notify(
        id: String, title: String, messageId: String,
        message: String, intent: Intent?, icon: Int, largeIcon: Bitmap? = null,
        tickerText: String? = null, doVibrateSound: Boolean = true
    ) {
        intent?.flags = Intent.FLAG_ACTIVITY_SINGLE_TOP or Intent.FLAG_ACTIVITY_CLEAR_TOP

        var info: NotificationInfo? = null

        synchronized(mNotificationInfos) {
            info = mNotificationInfos.firstOrNull { it.id == id }

            if (info != null) {
                // Need to remove, as it gets added later again!
                mNotificationInfos.remove(info)
            } else {
                info = NotificationInfo(id, icon = icon)
            }

            info?.title = title
            info?.intent = intent
            info?.icon = icon
            info?.largeIcon = largeIcon
            info?.tickerText = tickerText
            info?.doVibrateSound = doVibrateSound

            info?.putMessage(messageId, message)

            info?.let { mNotificationInfos.add(it) }
        }

        info?.let { notify(it) }
    }

    private fun notify(notificationInfo: NotificationInfo) {
        mNotificationManager?.notify(notificationInfo.idInt, notificationInfo.build())
    }

    @JvmOverloads
    fun getDefaultIntent(roomId: String? = null): Intent? {
        return mApp?.router?.main(mContext, openRoomId = roomId)
    }

    internal inner class Message(
        var id: String,

        var text: String
    )

    internal inner class NotificationInfo(
        var id: String? = null,

        var title: String? = null,

        var intent: Intent? = null,

        var icon: Int,

        var largeIcon: Bitmap? = null,

        var tickerText: String? = null,

        var doVibrateSound: Boolean = true
    ) {

        val idInt
            get() = id.hashCode()

        private val mMessages = arrayListOf<Message>()

        fun containsMessage(checkMsgId: String): Boolean {

            for (message in mMessages) {
                if (message.id == checkMsgId)
                    return true
            }

            return false
        }

        fun putMessage(id: String, text: String) {
            val m = mMessages.firstOrNull { it.id == id }

            if (m != null) {
                m.text = text
            } else {
                mMessages.add(Message(id, text))
            }
        }

        fun removeMessage(id: String) {
            mMessages.removeAll { it.id == id }
        }

        fun isEmpty(): Boolean {
            return mMessages.isEmpty()
        }

        fun build(): Notification {
            var flags = PendingIntent.FLAG_UPDATE_CURRENT

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                flags = flags or PendingIntent.FLAG_IMMUTABLE
            }

            val builder =
                NotificationCompat.Builder(mContext, KeanuConstants.NOTIFICATION_CHANNEL_ID_MESSAGE)

            builder
                .setSmallIcon(icon)
                .setTicker(tickerText)
                .setWhen(System.currentTimeMillis())
                .setLights(-0x670000, 300, 1000)
                .setContentTitle(title)
                .setContentText(mMessages.lastOrNull()?.text)
                .setContentIntent(
                    PendingIntent.getActivity(
                        mContext,
                        UNIQUE_INT_PER_CALL++,
                        intent,
                        flags
                    )
                )
                .setAutoCancel(true)

            if (doVibrateSound) {
                if (mApp?.preferenceProvider?.getNotificationState() == RoomNotificationState.MENTIONS_ONLY) {
                    if (mApp?.preferenceProvider?.isMentionNotificationSound() == true) {
                        playSound(mApp?.preferenceProvider?.getNotificationRingtone().toString())
                    }

                    if (mApp?.preferenceProvider?.isMentionNotificationVibrate() == true && isVibrateOn) {
                        startVibration()
                    }
                } else {
                    if (mApp?.preferenceProvider?.isNotificationSound() == true) {
                        playSound(mApp?.preferenceProvider?.getNotificationRingtone().toString())
                    }

                    if (mApp?.preferenceProvider?.isNotificationVibrate() == true && isVibrateOn) {
                        // Play vibration
                        startVibration()
                    }
                }
            }

            if (mMessages.isNotEmpty()) {
                // Sets the big view "big text" style and supplies the
                // text (the user's reminder message) that will be displayed
                // in the detail area of the expanded notification
                // These calls are ignored by the support library for
                // pre-4.1 devices.
                builder.setStyle(
                    NotificationCompat.BigTextStyle()
                        .bigText(mMessages.joinToString(separator = "\n") { it.text })
                )
            }

            if (largeIcon != null) builder.setLargeIcon(largeIcon)

            return builder.build()
        }
    }

    private var mAudioManager: AudioManager? = null

    private val isVibrateOn: Boolean
        get() {
            if (mAudioManager == null) mAudioManager =
                mContext.getSystemService(Context.AUDIO_SERVICE) as? AudioManager

            return when (mAudioManager?.ringerMode) {
                AudioManager.RINGER_MODE_NORMAL -> (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1
                        && Settings.System.getInt(
                    mContext.contentResolver,
                    Settings.System.VIBRATE_WHEN_RINGING,
                    0
                ) == 1)
                AudioManager.RINGER_MODE_SILENT -> false
                AudioManager.RINGER_MODE_VIBRATE -> true
                else -> false
            }
        }

    private fun startVibration() {
        // Play vibration
        if (mVibrator == null) {
            mVibrator =
                mContext.getSystemService(Context.VIBRATOR_SERVICE) as? Vibrator
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                mVibeEffect = VibrationEffect.createWaveform(VIBRATION_TIMINGS, -1)
            }
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            mVibrator?.vibrate(mVibeEffect)
        } else {
            @Suppress("DEPRECATION")
            mVibrator?.vibrate(VIBRATE_PATTERN, -1)
        }
    }

    private fun playSound(notificationSound: String) {
        try {
            // Play sound
            mContext.startService(
                mApp?.router?.soundService(
                    mContext,
                    notificationSound
                )
            )
        } catch (e: IllegalStateException) {
            Timber.e(e)
        }
    }
}