package info.guardianproject.keanu.core.ui.chatlist

import agency.tango.android.avatarview.AvatarPlaceholder
import android.content.Context
import android.util.AttributeSet
import android.widget.FrameLayout
import info.guardianproject.keanu.core.util.GlideUtils

class ChooseRoomItem(context: Context, attrs: AttributeSet?) : FrameLayout(context, attrs) {

    private var mRoomName: String? = null
    private var mLastAvatarUrl: String? = null

    fun bind(holder: ChooseRoomItemHolder, address: String, roomName: String?, avatarUrl: String?) {
        mRoomName = roomName ?: address.split(":".toRegex()).toTypedArray()[0]
        holder.line1.text = mRoomName

        if (avatarUrl == null) {
            holder.avatar.setImageDrawable(AvatarPlaceholder(roomName))
        }
        else if (mLastAvatarUrl?.equals(avatarUrl) != true) {
            mLastAvatarUrl = avatarUrl
            GlideUtils.loadAvatar(context, mLastAvatarUrl, AvatarPlaceholder(roomName), holder.avatar)
        }

        holder.line1.visibility = VISIBLE
    }

}