package info.guardianproject.keanuapp.ui

import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.WindowManager
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.preference.PreferenceManager
import info.guardianproject.keanu.core.ImApp
import info.guardianproject.keanu.core.Preferences
import info.guardianproject.keanu.core.ui.me.providers.PreferenceProvider
import info.guardianproject.keanuapp.R
import org.matrix.android.sdk.api.session.Session

/**
 * Created by n8fr8 on 5/7/16.
 */
open class BaseActivity : AppCompatActivity() {

    protected val mApp by lazy { application as ImApp}
    protected open val mSession: Session? by lazy { mApp.matrixSession }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val preferenceProvider = PreferenceProvider(applicationContext)
        if (preferenceProvider.getScreenshotInfo()) window.setFlags(
            WindowManager.LayoutParams.FLAG_SECURE,
            WindowManager.LayoutParams.FLAG_SECURE
        )

        //not set color
        val settings = PreferenceManager.getDefaultSharedPreferences(this)
        val themeColorHeader = settings.getInt(Preferences.THEME_COLOR_HEADER, -1)
        val themeColorBg = settings.getInt(Preferences.THEME_COLOR_BG, -1)
        if (themeColorHeader != -1) {
            window.navigationBarColor = themeColorHeader
            window.statusBarColor = themeColorHeader
            if (supportActionBar != null) {
                supportActionBar!!.setBackgroundDrawable(ColorDrawable(themeColorHeader))
            }
        }
        if (themeColorBg != -1) {
            window.decorView.setBackgroundColor(themeColorBg)
        }
    }

    open fun applyStyleForToolbar() {
        val preferenceProvider = PreferenceProvider(applicationContext)
        val themeColorHeader = preferenceProvider.getHeaderColor()
        val themeColorText = preferenceProvider.getTextColor()
        val toolbar = findViewById<Toolbar>(R.id.toolbar)
        if (themeColorHeader != -1) {
            toolbar.setBackgroundColor(themeColorHeader)
            toolbar.setTitleTextColor(themeColorText)
        }
    }

    fun showToast(msg: String) {
        Toast.makeText(applicationContext, msg, Toast.LENGTH_LONG).show()
    }
}