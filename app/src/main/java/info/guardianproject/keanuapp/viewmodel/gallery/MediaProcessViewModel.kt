package info.guardianproject.keanuapp.viewmodel.gallery

import android.graphics.BitmapFactory
import android.net.Uri
import androidx.documentfile.provider.DocumentFile
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import info.guardianproject.keanu.core.ImApp
import info.guardianproject.keanu.core.util.BitmapHelper.saveAsJPG
import info.guardianproject.keanu.core.util.SecureMediaStore
import info.guardianproject.keanuapp.viewmodel.BaseViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import timber.log.Timber
import java.io.FileOutputStream
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.Date
import java.util.Locale

class MediaProcessViewModel: BaseViewModel() {
    private val mSaveMediaLiveData = MutableLiveData<String?>()
    val observableSaveMediaLiveData = mSaveMediaLiveData as LiveData<String?>

    fun savePhotoToGallery(mimeType: String, fileUri: Uri, saveToExternalStorage: Boolean, customUri: Uri? = null) {

        viewModelScope.launch(Dispatchers.IO) {
            val appContext = ImApp.sImApp?.applicationContext ?: return@launch

            if (customUri != null) {
                val documentFile = DocumentFile.fromTreeUri(appContext, customUri)
                val targetFileName = fileUri.lastPathSegment ?: (System.currentTimeMillis().toString() + ".jpg")
                val file = documentFile?.createFile(mimeType, targetFileName)
                val outFile = file?.uri?.let { uri ->
                    appContext.contentResolver.openFileDescriptor(uri, "w")
                } ?: return@launch

                val fos = FileOutputStream(outFile.fileDescriptor)
                try {
                    SecureMediaStore.exportContent(fileUri, fos)
                    mSaveMediaLiveData.postValue(targetFileName)
                } catch (e: IOException) {
                    Timber.e("savePhotoToGallery failed: %s", e)
                    mSaveMediaLiveData.postValue(null)
                    file.delete()
                }

            } else {
                val exportPath = SecureMediaStore.exportPath(appContext, mimeType, fileUri, saveToExternalStorage)
                try {
                    SecureMediaStore.exportContent(mimeType, fileUri, exportPath)
                    val imageBmp = BitmapFactory.decodeFile(exportPath.absolutePath)
                    imageBmp.saveAsJPG(exportPath.name)
                    mSaveMediaLiveData.postValue(exportPath.name)
                } catch (e: IOException) {
                    Timber.e(e)
                    mSaveMediaLiveData.postValue(null)
                }
            }
        }
    }

    fun saveVideoFile(sourceUri: Uri?, destUri: Uri, mime: String?) {
        val appContext = ImApp.sImApp?.applicationContext ?: return
        viewModelScope.launch(Dispatchers.IO) {
            val documentFile = DocumentFile.fromTreeUri(appContext, destUri)

            sourceUri?.path?.let { path ->
                val extension = path.substring(path.lastIndexOf("."))
                val df = SimpleDateFormat("yyyy_MM_dd_HH_mm_ss", Locale.US).format(Date())
                val filename = "Keanu_$df$extension"
                val mimeType = mime ?: return@let

                val file = documentFile?.createFile(mimeType, filename)
                val outFile = file?.uri?.let { uri ->
                    appContext.contentResolver.openFileDescriptor(uri, "w")
                } ?: return@let

                val fos = FileOutputStream(outFile.fileDescriptor)
                try {
                    SecureMediaStore.exportContent(sourceUri, fos)
                    mSaveMediaLiveData.postValue(filename)
                } catch (e: IOException) {
                    mSaveMediaLiveData.postValue(null)
                    file.delete()
                }
            }
        }
    }
}